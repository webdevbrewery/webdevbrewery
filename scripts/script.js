$(document).ready( function() {

  // Console Title
  consoleText(['WebDev Brewery'], 'text');

  function consoleText(words, id) {
    var visible = true;
    var con = document.getElementById('console');
    var letterCount = 1;
    var x = 1;
    var waiting = false;
    var target = document.getElementById(id)
    window.setInterval(function() {

      if (letterCount === 0 && waiting === false) {
        waiting = true;
        target.innerHTML = words[0].substring(0, letterCount)
        window.setTimeout(function() {
          x = 1;
          letterCount += x;
          waiting = false;
        }, 1000)
      } else if (waiting === false) {
        target.innerHTML = words[0].substring(0, letterCount)
        letterCount += x;
      }
    }, 120)
    window.setInterval(function() {
      if (visible === true) {
        con.className = 'console-underscore hidden'
        visible = false;

      } else {
        con.className = 'console-underscore'

        visible = true;
      }
    }, 400)
  }

  // Dark and Light Modes
  let displayMode = "light"



      $('#displayModeBtn').on('change', function (){
          if (displayMode == "light"){
              $('.mode').removeClass("bg-white text-dark");
              $('.mode').addClass("bg-dark text-white");
              $('.mode-soft').css('color', '#575859');
              $('.mode-hard').css('color', '#F4F4F4');
              $('.modal-body').css('box-shadow', '0 0 20px black');
              displayMode = "dark";
          } else {
              $('.mode').removeClass("bg-dark text-white");
              $('.mode').addClass("bg-white text-dark");
              $('.mode-soft').css('color', '#cccccc');
              $('.mode-hard').css('color', 'dimgray');
              $('.modal-body').css('box-shadow', '0 0 20px gray');
              displayMode = "light";
          }
      })

      $(".modal").click(function(ev){
          if(ev.target != this) return;
          $('.modal').modal('hide');
      });

      $("#aboutModal").on('click', function (e){
          $("#aboutModal").modal('hide');
      })

      $(".my-flipster").flipster({
          style: 'carousel',
          spacing: 0,
          nav: false,
          buttons: true,
          start: 0,
          loop: true,
      });

      // ——————————————————————————————————————————————————
  // TextScramble
  // ——————————————————————————————————————————————————

  class TextScramble {
      constructor(el) {
        this.el = el
        this.chars = '!<>-_\\/[]{}—=+*^?#__'
        this.update = this.update.bind(this)
      }
      setText(newText) {
        const oldText = this.el.innerText
        const length = Math.max(oldText.length, newText.length)
        const promise = new Promise((resolve) => this.resolve = resolve)
        this.queue = []
        for (let i = 0; i < length; i++) {
          const from = oldText[i] || ''
          const to = newText[i] || ''
          const start = Math.floor(Math.random() * 40)
          const end = start + Math.floor(Math.random() * 40)
          this.queue.push({ from, to, start, end })
        }
        cancelAnimationFrame(this.frameRequest)
        this.frame = 0
        this.update()
        return promise
      }
      update() {
        let output = ''
        let complete = 0
        for (let i = 0, n = this.queue.length; i < n; i++) {
          let { from, to, start, end, char } = this.queue[i]
          if (this.frame >= end) {
            complete++
            output += to
          } else if (this.frame >= start) {
            if (!char || Math.random() < 0.28) {
              char = this.randomChar()
              this.queue[i].char = char
            }
            output += `<span class="dud">${char}</span>`
          } else {
            output += from
          }
        }
        this.el.innerHTML = output
        if (complete === this.queue.length) {
          this.resolve()
        } else {
          this.frameRequest = requestAnimationFrame(this.update)
          this.frame++
        }
      }
      randomChar() {
        return this.chars[Math.floor(Math.random() * this.chars.length)]
      }
    }
    
    // ——————————————————————————————————————————————————
    // Text
    // ——————————————————————————————————————————————————
    
    const phrases = [
      'Bring your brand to the digital world',
      'Solutions tailored for your needs'
    ]
    
    const el = document.querySelector('.text')
    const fx = new TextScramble(el)
    
    let counter = 0
    const next = () => {
      fx.setText(phrases[counter]).then(() => {
        setTimeout(next, 3000)
      })
      counter = (counter + 1) % phrases.length
    }
    
    next()
      
})

